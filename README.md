# Game of Life #
This is a basic Python implementation of Conway's Game of Life. To learn more about the logic and philosophy on the Game, click [here](http://en.wikipedia.org/wiki/Conway's_Game_of_Life). 

## Features
* Start a new simulation and watch the evolution of the population
* Pause and resume the simulation
* Reset the simulation
* Customization of the game (see section "Usage" for more information)

## Installation
* Clone this repository into your project
* Install requirements using the requirements.txt file

## Usage
Create a new torus. The torus is the world in which your cells live.

```
from game import game_logic
torus = game_logic.Torus()
```

You can customize the torus by passing the following keyword parameters to the constructor:

* **width**: width of the torus in number of cells, default is 30
* **height**: height of the torus in number of cells, default is 30
* **living_ratio**: value between 0 and 1 determining the probability, that cell is alive in initial round, default it 0.6

To start the game, construct a new `GameOfLife` object passing the created torus:
```
from game import user_interface, game_logic
game = user_interface.GameOfLife(torus)
```

You can customize the graphical representation of the simulation by passing the following keyword parameters to the constructor:

* **offset**: the offset between canvas and frame in pixels, default is 2 
* **pixel_size**: size of a cell in pixels, default is 20
* **speed**: time in milliseconds passing between two simulation rounds, default is 500 

See `sample_run.py` for sample usage.
## Known Issues