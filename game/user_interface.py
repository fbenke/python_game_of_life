from sys import argv
from Tkinter import Tk, Canvas, Frame, BOTH, Button, RIGHT
from game_logic import Torus


class GameOfLife(Frame):

    def __init__(self, torus, offset=2, pixel_size=20, speed=500):

        self._parent = Tk()
        self._torus = torus
        self._offset = offset

        self._pixel_size = pixel_size
        self._speed = speed

        self._interrupt = False

        Frame.__init__(self, self._parent)
        self._init_ui_()

    def _init_ui_(self):

        canvas_width = 2 * self._offset + self._pixel_size * self._torus.width()
        canvas_height = 2 * self._offset + self._pixel_size * self._torus.height()

        self._parent.title("Game of Life")

        self.pack(fill=BOTH, expand=1)

        self._canvas = Canvas(self._parent, height=canvas_height, width=canvas_width)

        for i in xrange(self._torus.height()):
            for j in xrange(self._torus.width()):

                if self._torus.cell_type(i, j):
                    self._new_rectangle_(i, j, "black")
                else:
                    self._new_rectangle_(i, j, "white")

        self._canvas.pack(padx=self._offset, pady=self._offset)

        self._start_button = Button(self._parent, text="Stop", command=self._callback_start_)
        self._reset_button = Button(self._parent, text="Reset", command=self._callback_reset_)

        self._start_button.pack(side=RIGHT, padx=10, pady=5)
        self._reset_button.pack(side=RIGHT)

        self._parent.after(self._speed, self._update_ui_)
        self._parent.mainloop()

    def _callback_start_(self):
        """
        pauses or resume the game
        """
        if not self._interrupt:
            self._interrupt = True
            self._start_button.config(text="Start")
        else:
            self._interrupt = False
            self._start_button.config(text="Stop")
            self._update_ui_()

    def _callback_reset_(self):
        """
        reset the game
        """
        self._torus = Torus(self._torus.width(), self._torus.height())
        self._update_ui_(update=False)

    def _new_rectangle_(self, i, j, appearance):
        x_1 = self._offset+j*self._pixel_size,
        y_1 = self._offset+i*self._pixel_size
        x_2 = self._offset+j*self._pixel_size+self._pixel_size
        y_2 = self._offset+i*self._pixel_size+self._pixel_size
        self._canvas.create_rectangle(x_1, y_1, x_2, y_2, fill=appearance, tags="cell")

    def _update_ui_(self, update=True):

        if self._interrupt:
            return

        if update:
            self._torus.next_round()

        items = self._canvas.find_withtag("cell")

        for i in xrange(self._torus.height()):
            for j in xrange(self._torus.width()):
                index = self._torus.width() * i + j
                if self._torus.cell_type(i, j):
                    self._canvas.itemconfig(items[index], fill="black")
                else:
                    self._canvas.itemconfig(items[index], fill="white")

        self._parent.after(self._speed, self._update_ui_)
