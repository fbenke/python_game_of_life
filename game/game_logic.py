import random


class Torus(object):
    def __init__(self, width=30, height=30, living_ratio=0.6):
        self._torus = [
            [True if random.random() < living_ratio else False for _ in xrange(width)]
            for _ in xrange(height)
        ]

    def next_round(self):
        self._torus = [
            [self._update_cell_(x, y) for y in xrange(len(self._torus[0]))]
            for x in xrange(len(self._torus))
        ]

    def cell_type(self, x, y):
        return self._torus[x][y]

    def width(self):
        return len(self._torus[0])

    def height(self):
        return len(self._torus)

    def _count_neighbours_(self, x, y):
        count = 0

        xMinus = len(self._torus)-1 if x-1 < 0 else x - 1
        yMinus = len(self._torus[0])-1 if y-1 < 0 else y - 1
        xPlus = 0 if x+1 == len(self._torus) else x + 1
        yPlus = 0 if y+1 == len(self._torus[0]) else y + 1

        if self._torus[xMinus][yMinus]:
            count += 1
        if self._torus[xMinus][y]:
            count += 1
        if self._torus[xMinus][yPlus]:
            count += 1
        if self._torus[x][yMinus]:
            count += 1
        if self._torus[x][yPlus]:
            count += 1
        if self._torus[xPlus][yMinus]:
            count += 1
        if self._torus[xPlus][y]:
            count += 1
        if self._torus[xPlus][yPlus]:
            count += 1

        return count

    def _update_cell_(self, x, y):
        if self._torus[x][y]:
            return True if self._count_neighbours_(x, y) in (2, 3) else False
        else:
            return True if self._count_neighbours_(x, y) == 3 else False

    def fixed_torus(self):
        """
        creates a deterministic torus for testing purposes
        """
        self._torus = init_torus(4, 5, 0)

        self._torus[0][0] = False
        self._torus[0][1] = True
        self._torus[0][2] = True
        self._torus[0][3] = False

        self._torus[1][0] = False
        self._torus[1][1] = False
        self._torus[1][2] = True
        self._torus[1][3] = False

        self._torus[2][0] = False
        self._torus[2][1] = True
        self._torus[2][2] = False
        self._torus[2][3] = True

        self._torus[3][0] = True
        self._torus[3][1] = False
        self._torus[3][2] = False
        self._torus[3][3] = True

        self._torus[4][0] = False
        self._torus[4][1] = False
        self._torus[4][2] = False
        self._torus[4][3] = True
