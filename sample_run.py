from sys import argv
from game import user_interface, game_logic

def main():
	if len(argv) == 1:
		torus = game_logic.Torus()
	elif len(argv) == 3:
		_, dim_x, dim_y = argv
		torus = game_logic.Torus(int(dim_x), int(dim_y))
	elif len(argv) == 4:
		print argv
		_, dim_x, dim_y, living_cells = argv
		torus = game_logic.Torus(int(dim_x), int(dim_y), float(living_cells))

	game = user_interface.GameOfLife(torus)

if __name__ == '__main__':
  main()